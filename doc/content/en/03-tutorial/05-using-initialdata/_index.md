---
title: Using initialData
url: tutorial/using-initialdata
description: Details about how use initialData section.
weight: 5
chapter: true
---

The initialData section from [SGCluster]({{% relref "06-crd-reference/01-sgcluster" %}}) allows you to initialize database objects from a valid `SQL` scripts or even initialize a cluster from an existing backups.

To achieve this you have the next options:

{{% children style="li" depth="2" description="true" %}}